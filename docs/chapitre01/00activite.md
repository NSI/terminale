# Activité de découverte

!!! info "Exercice 1"
    Quel est l'intrus dans cette liste ?

    - une douzaine d’huîtres
    - un citron
    - un pain
    - un rayon de soleil
    - une lame de fond
    - six musiciens
    - une porte avec son paillasson
    - un monsieur décoré de la légion d'honneur
    - un autre raton laveur
